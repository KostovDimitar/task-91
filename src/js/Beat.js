import EventEmitter from "eventemitter3";
import Application from "./Application";

export default class Beat extends EventEmitter {
    static get events() {
        return {
            BIT: "bit",
        };
    }

    constructor() {
        super();
        setInterval(() => {
            this.emit(Beat.events.BIT);
        }, 1000)
        
    }
}