import EventEmitter from "eventemitter3";
import Beat from "./Beat";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.lyrics = ["Ah", "ha", "ha", "ha", "stayin' alive", "stayin' alive"];
    this.count = 0;
    this._beat = new Beat(() => {});
    this._beat.on(Beat.events.BIT, () => {
      this._create();
    });
    
    this.emit(Application.events.READY);
  }

  _create() {
    for (let index = 0; index < this.lyrics.length; index++) {
      let main = document.querySelector(".main");
      let message = document.createElement("p");
      message.setAttribute("class", "message");
      message.innerText = this.lyrics[index];
      main.appendChild(message)
    }
      
  }
}
      